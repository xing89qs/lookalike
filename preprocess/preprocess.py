#!/usr/bin/env python
# -- coding:utf-8 --

import mmap
import time
import pandas as pd

single_values = set(['uid', 'age', 'gender', 'marriageStatus', 'education', 'consumptionAbility', 'LBS', 'ct', 'os',
                     'carrier', 'house'])
multi_values = set(['interest1', 'interest2', 'interest3', 'interest4', 'interest5', 'kw1', 'kw2', 'kw3',
                    'topic1', 'topic2', 'topic3', 'appIdInstall', 'appIdAction'])

line_cnt = 0
dicts_single = []
dicts_multi = []
for l in open("../data/userFeature.data", "r+b"):
    l = l.strip()
    schemas = l.split('|')
    dict_single = {}
    dict_multi = {}
    for schema in schemas:
        schema = schema.split(' ')
        if schema[0] == 'uid':
            dict_single['uid'] = dict_multi['uid'] = '-'.join(schema[1:])
        if schema[0] in single_values:
            dict_single[schema[0]] = '-'.join(schema[1:])
        else:
            dict_multi[schema[0]] = '-'.join(schema[1:])
    if line_cnt % 100000 == 0:
        print line_cnt
    dicts_single.append(dict_single)
    dicts_multi.append(dict_multi)
    line_cnt += 1
print 'total: ', line_cnt

df_single = pd.DataFrame(dicts_single)
df_single.to_csv('../data/userFeatureSingle.csv', index=False)
del df_single
del dicts_single
df_multi = pd.DataFrame(dicts_multi)
df_multi.to_csv('../data/userFeatureMulti.csv', index=False)
