#!/usr/bin/env python
# -- coding:utf-8 --

import sys

sys.path.append('..')
import os
import pandas as pd
from scipy.sparse import csr_matrix, hstack, vstack
from sklearn.datasets import load_svmlight_file, dump_svmlight_file
import numpy as np
import tools
from sklearn.externals import joblib
import gc

DATA_SET_ROOT = os.path.split(os.path.realpath(__file__))[0] + '/../feature_pool'


def list_datasets():
    if not os.path.exists(DATA_SET_ROOT):
        print ('feature_pool文件夹不存在')
    for dir in os.listdir(DATA_SET_ROOT):
        if os.path.isdir(DATA_SET_ROOT + dir):
            print (dir)


def create(dataset, y_frame):
    assert (type(y_frame) == type(pd.DataFrame()))
    if not os.path.exists(DATA_SET_ROOT):
        os.mkdir(DATA_SET_ROOT)
    confirm_columns = ['uid', 'aid']
    for column in confirm_columns:
        if column not in y_frame.columns:
            print ('Error: y_frame缺少列 %s' % (column))
            return

    if not os.path.exists(DATA_SET_ROOT + '/' + dataset):
        os.mkdir(DATA_SET_ROOT + '/' + dataset)
    y_frame.to_csv(DATA_SET_ROOT + '/' + dataset + '/label.csv', index=False)
    print ('生成dataset: %s' % (dataset))
    print ('共 %d 条样本' % (len(y_frame)))
    return DataSet(dataset)
    pass


def load(set_name):
    return DataSet(set_name)


def load_type(set_name, save_file_type):
    return DataSet(set_name, save_file_type)


def concat(datasets, features):
    return vstack([dataset[features] for dataset in datasets]).tocsr(copy=False), pd.concat(
        [dataset.y for dataset in datasets])


class DataSet:
    def __init__(self, dataset, save_file_type='npz'):
        '''
        :param dataset:
        :param save_file_type: svm或者是npz, 以svm格式保存还是直接用npz格式保存 ,后者占内存多但速度快
        :return:
        '''
        if not os.path.exists(DATA_SET_ROOT + '/' + dataset) or str(dataset).strip() == '':
            raise Exception('错误! dataset %s 不存在' % (dataset))
            pass
        if not os.path.exists(DATA_SET_ROOT + '/' + dataset + '/label.csv'):
            raise Exception('错误! label.csv文件不存在')
        self.PATH = DATA_SET_ROOT + '/' + dataset
        self.y = pd.read_csv(DATA_SET_ROOT + '/' + dataset + '/label.csv')
        self.feature_pool = {}
        self.save_file_type = save_file_type
        pass

    def __getitem__(self, item):
        if type(item) != list:
            item = [item]
        item = [self.read_feature(x) for x in item]
        if len(item) == 1:
            return item[0]
        return self.__dfs(0, len(item) - 1, item)

    def __setitem__(self, key, value):
        self.feature_pool[key] = value

    def __len__(self):
        return len(self.y)

    def exists_common_model(self, filename):
        return os.path.exists(DATA_SET_ROOT + '/common/' + filename)

    def save_common_model(self, model, filename):
        joblib.dump(model, DATA_SET_ROOT + '/common/' + filename)

    def load_common_model(self, filename):
        return joblib.load(DATA_SET_ROOT + '/common/' + filename)

    def read_feature(self, fname):
        if fname == 'label':
            return self.y['label']
        elif fname in self.feature_pool:
            return self.feature_pool[fname]
        else:
            feature_file = self.PATH + '/' + fname + '.' + ('svm' if self.save_file_type == 'svm' else 'npz')
            if not os.path.exists(feature_file):
                raise Exception('特征 %s 不存在!' % (fname,))
            if self.save_file_type == 'svm':
                X, Y = load_svmlight_file(feature_file)
            else:
                X = tools.load_sparse_csr(feature_file)
            self.feature_pool[fname] = X
            return X
        pass

    def __read_dense_feature(self, fname, features):
        if fname == 'label':
            return self.y['label'].as_matrix()
        elif fname in self.feature_pool:
            return self.feature_pool[fname]
        else:
            feature_file = self.PATH + '/' + fname + '.' + ('svm' if self.save_file_type == 'svm' else 'npz')
            if not os.path.exists(feature_file):
                raise Exception('特征 %s 不存在!' % (fname,))
            X, fmap = tools.load_dense_csr(feature_file)
            frame = pd.DataFrame(data=X.toarray(), columns=fmap)
            if features is not None:
                need_features = set(frame.columns) & set(features)
            else:
                need_features = set(frame.columns)
            frame = frame[list(need_features)]
            self.feature_pool[fname] = frame
            return frame
        pass

    def read_dense_feature(self, feature_list, features):
        return self.__dfs_dense(0, len(feature_list) - 1, feature_list, features)

    def __dfs_dense(self, l, r, feature_list, features):
        '''
        if l == r:
            return self.__read_dense_feature(feature_list[l], features)
        else:
            mid = (l + r) / 2
            result_l = self.__dfs_dense(l, mid, feature_list, features)
            result_r = self.__dfs_dense(mid + 1, r, feature_list, features)
            ret = result_l.join(result_r)
            del result_l
            del result_r
            return ret
        '''
        return pd.concat([self.__read_dense_feature(feature_list[i], features) for i in range(l, r + 1)], axis=1)

    def __dfs(self, l, r, features):
        if l == r:
            return features[l]
        else:
            mid = (l + r) / 2
            result_l = self.__dfs(l, mid, features)
            result_r = self.__dfs(mid + 1, r, features)
            ret = hstack([result_l, result_r]).tocsr(copy=False)
            del result_l
            del result_r
            gc.collect()
            return ret
            # return hstack([features[i] for i in range(l, r + 1)]).tocsr(copy=False)

    def write_feature(self, fname, features):
        '''
        :param fname:
        :param features:
        :return:
        '''
        if type(features) != list:
            raise Exception('features 必须是一个list')
        final_result = self.__dfs(0, len(features) - 1, features).tocsr()
        if final_result.shape[0] != len(self.y):
            raise Exception('样本数错误! label表有 %d 条样本, 输入有 %d 条样本.' % (len(self.y), final_result.shape[0]))
        if self.save_file_type == 'npz':
            tools.save_sparse_csr(self.PATH + '/' + fname + '.npz', final_result)
        else:
            dump_svmlight_file(final_result, self.y['label'].as_matrix(), self.PATH + '/' + fname + '.svm')
        print ('创建特征文件 %s 成功, 共 %d 维特征' % (fname, final_result.shape[1]))
        pass

    def write_dense_feature(self, fname, feature, fmap):
        '''
        :param fname:
        :param features:
        :return:
        '''
        final_result = feature
        if final_result.shape[0] != len(self.y):
            raise Exception('样本数错误! label表有 %d 条样本, 输入有 %d 条样本.' % (len(self.y), final_result.shape[0]))
        if self.save_file_type == 'npz':
            tools.save_dense_csr(self.PATH + '/' + fname + '.npz', final_result, fmap)
        else:
            dump_svmlight_file(final_result, self.y['label'].as_matrix(), self.PATH + '/' + fname + '.svm')
        print ('创建特征文件 %s 成功, 共 %d 维特征' % (fname, final_result.shape[1]))
        pass
