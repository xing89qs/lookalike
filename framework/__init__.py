#!/usr/bin/env python
# -- coding:utf-8 --

from framework.dataset import *

import platform

IS_LOCAL = ('linux' not in platform.system().lower())
