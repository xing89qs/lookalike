#!/usr/bin/env python
# -- coding:utf-8 --

import framework
import pandas as pd
import numpy as np

np.random.seed(0)
train_frame = pd.read_csv('data/train.csv')
train_frame['label'] = train_frame['label'].apply(lambda x: 1 if x == 1 else 0)
if framework.IS_LOCAL:

    temp = np.random.rand(len(train_frame))
    mask = temp < 0.07
    mask1 = (temp >= 0.07) & (temp < 0.1)
    framework.create('train_0', train_frame[mask])
    framework.create('eval_0', train_frame[mask1])
else:
    temp = np.random.rand(len(train_frame))
    mask = temp < 0.7
    mask1 = (~mask)
    framework.create('train_0', train_frame[mask])
    framework.create('eval_0', train_frame[mask1])
    framework.create('train', train_frame)
    test_frame = pd.read_csv('data/test1.csv')
    test_frame['label'] = 0
    framework.create('test', test_frame)
