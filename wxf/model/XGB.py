#!/usr/bin/env python
# -- coding:utf-8 --

import sys

sys.path.append('..')
sys.path.append('../..')

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import log_loss
import framework
from xgboost import XGBClassifier
import xgboost as xgb
import tools
import pandas as pd
import numpy as np
import zipfile


def read_set(set_name, feature_files, features):
    data_sets = [framework.load(name) for name in set_name.split(',')]
    X = pd.concat([data_set.read_dense_feature(feature_files, features) for data_set in data_sets])
    Y = pd.concat([data_set.y for data_set in data_sets])
    return X, Y


def submit(df, filename):
    df.sort_values("instanceID", inplace=True)
    df['prob'] = df['pred']
    df[['instanceID', 'prob']].to_csv("submission.csv", index=False)
    with zipfile.ZipFile(filename, "w") as fout:
        fout.write("submission.csv", compress_type=zipfile.ZIP_DEFLATED)


def day_weight(x):
    return 1


def run(train_name, test_name, feature_files, submit_file=None, select_feature='xgb.f'):
    params = {
        'booster': 'gbtree',
        'objective': 'binary:logistic',
        'eta': 0.1,
        'max_depth': 10,
        'subsample': 0.8,
        'colsample_bytree': 0.6,
        'silent': 1,
        'eval_metric': 'auc',
        'nthread': -1,
        'min_child_weight': 100,
    }
    features = tools.read_feature(select_feature)
    print len(features)

    trainX, trainY = read_set(train_name, feature_files, features)
    testX, testY = read_set(test_name, feature_files, features)

    print len(trainY[trainY['label'] == 1]), len(trainY[trainY['label'] == 0])
    print len(testY[testY['label'] == 1]), len(testY[testY['label'] == 0])

    dtrain = xgb.DMatrix(data=trainX[features].as_matrix(), label=trainY['label'].as_matrix())
    dtest = xgb.DMatrix(data=testX[features].as_matrix(), label=testY['label'].as_matrix())
    del trainX
    del testX

    watchlist = [(dtrain, 'train'), (dtest, 'test')]
    model = xgb.train(params, dtrain, num_boost_round=300, evals=watchlist, verbose_eval=10, )
    testY['pred'] = model.predict(dtest)

    mapFeat = dict(zip(["f" + str(i) for i in range(len(features))], features))
    ts = pd.Series(model.get_fscore())
    ts.index = ts.reset_index()['index'].map(mapFeat)
    with open('best_features_' + train_name + '.feature', 'w') as file:
        for f in ts.order().index[-300:]:
            file.write(str(f) + ',' + str(ts[f]) + '\n')

            # if submit_file is not None:
            #    submit(testY, submit_file)


if __name__ == '__main__':
    features = ['ad_label_stat']

    run(sys.argv[1], sys.argv[2], features,
        None if len(sys.argv) <= 3 else sys.argv[3], select_feature='xgb.f' if len(sys.argv) <= 4 else sys.argv[4])
