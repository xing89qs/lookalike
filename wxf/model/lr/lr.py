#!/usr/bin/env python
# -- coding:utf-8 --


import tensorflow as tf
from scipy.sparse import coo_matrix
import numpy as np


class LR(object):
    EPS = 1e-10

    def __init__(self, learning_rate=0.1, training_epochs=10, display_step=1):
        # Parameters
        self.learning_rate = learning_rate
        self.training_epochs = training_epochs
        self.display_step = display_step
        self.tensor_map = {}
        pass

    def _init_network(self, shape_X, shapeY):
        # tf Graph Input
        self.tensor_map['input'] = x = tf.sparse_placeholder(tf.float32, [None, shape_X[1]])
        self.tensor_map['output'] = y = tf.placeholder(tf.float32, [None, ])

        # Set model weights
        self.tensor_map['w'] = w = tf.get_variable('w', shape=[shape_X[1], 1],
                                                   initializer=tf.truncated_normal_initializer(mean=0, stddev=1e-2))
        self.tensor_map['b'] = b = tf.get_variable('b', shape=[1], initializer=tf.zeros_initializer())

        pred_val = tf.reshape(tf.add(tf.sparse_tensor_dense_matmul(x, w), b))
        self.tensor_map['pred'] = tf.sigmoid(pred_val)
        _, self.tensor_map['roc_score'] = tf.metrics.auc(y, self.tensor_map['pred'])
        self.tensor_map['cost'] = cost = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(logits=pred_val, labels=y))
        self.tensor_map['optimizer'] = tf.train.GradientDescentOptimizer(self.learning_rate).minimize(cost)
        pass

    def fit(self, X, y, sample_weight=None, batch_size=1024):
        self._init_network((X.shape[0], X.shape[1]), 1)

        # Start training
        self.session = sess = tf.Session()  # Run the initializer
        # Initialize the variables (i.e. assign their default value)
        init = tf.global_variables_initializer()
        sess.run(init)
        sess.run(tf.initialize_local_variables())
        total_batch = int((X.shape[0] + batch_size - 1) / batch_size)

        # Training cycle
        for epoch in range(self.training_epochs):
            avg_cost = 0.

            print total_batch
            # Loop over all batches
            for i in range(total_batch):
                batch_xs, batch_ys = X[i * batch_size:(i + 1) * batch_size], y[i * batch_size:(i + 1) * batch_size]
                # Run optimization op (backprop) and cost op (to get loss value)
                temp = coo_matrix(batch_xs)
                _, c, preds, w = sess.run([optimizer, cost, pred, self.w1],
                                          feed_dict={input: (np.array([temp.row, temp.col]).T, temp.data, temp.shape),
                                                     output: batch_ys})
                print w
                # Compute average loss
                avg_cost += c / total_batch
                print 'Round', i
                # Display logs per epoch step
            if (epoch + 1) % self.display_step == 0:
                print("Epoch:", '%04d' % (epoch + 1), "cost=", "{:.9f}".format(avg_cost))

        print("Optimization Finished!")

    pass

    def predict(self, X):
        pass

    def evaluate(self, X, y):
        sess = self.session
        temp = coo_matrix(X)  # Test model
        print sess.run(self.roc_score, feed_dict={self.input: (np.array([temp.row, temp.col]).T, temp.data, temp.shape),
                                                  self.output: y})

        print 'Predict:', sess.run(self.pred,
                                   feed_dict={self.input: (np.array([temp.row, temp.col]).T, temp.data, temp.shape)})

    def close(self):
        self.session.close()
