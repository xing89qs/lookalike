#!/usr/bin/env python
# -- coding:utf-8 --


import tensorflow as tf
from scipy.sparse import coo_matrix
import numpy as np


class FM(object):
    def __init__(self, learning_rate=0.3, training_epochs=10, display_step=1):
        # Parameters
        self.learning_rate = learning_rate
        self.training_epochs = training_epochs
        self.display_step = display_step
        pass

    def _init_network(self, shape_X, shapeY):
        # tf Graph Input
        x = tf.sparse_placeholder(tf.float32, [None, shape_X[1]])
        y = tf.placeholder(tf.float32, [None, ])

        # Set model weights
        self.w1 = w1 = tf.get_variable('w1', shape=[shape_X[1], 1],
                                       initializer=tf.truncated_normal_initializer(mean=0, stddev=1e-2))
        b = tf.get_variable('bias', shape=[1],
                            initializer=tf.zeros_initializer())

        t1 = tf.sparse_tensor_dense_matmul(x, w1)
        # Construct model
        pred = tf.add(t1, b)

        v = tf.get_variable('v', shape=[shape_X[1], 4],
                            initializer=tf.truncated_normal_initializer(mean=0, stddev=0.01))

        item1 = tf.sparse_tensor_dense_matmul(x, v)

        new1 = tf.SparseTensor(indices=x.indices, values=tf.pow(x.values, 2), dense_shape=x.dense_shape)
        item2 = tf.sparse_tensor_dense_matmul(new1, tf.pow(v, 2))

        # shape of [None, 1]
        interaction_terms = tf.multiply(0.5, tf.reduce_mean(tf.subtract(item1, item2), 1, keep_dims=True))

        # pred = tf.nn.softmax(tf.add(t1, b))  # Softmax
        pred = tf.add(pred, interaction_terms)
        pred = tf.reshape(pred, shape=[-1])

        cost = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=pred, labels=y))
        # Gradient Descent
        optimizer = tf.train.GradientDescentOptimizer(self.learning_rate).minimize(cost)
        return x, y, pred, cost, optimizer
        pass

    def fit(self, X, y, sample_weight=None, batch_size=1024, eval=None):
        input, output, pred, cost, optimizer = self._init_network((X.shape[0], X.shape[1]), 1)
        self.input = input
        self.output = output
        self.pred = pred
        self.cost = cost
        self.optimizer = optimizer
        _, self.roc_score = tf.metrics.auc(self.output, tf.sigmoid(self.pred))

        # Start training
        self.session = sess = tf.Session()  # Run the initializer
        # Initialize the variables (i.e. assign their default value)
        init = tf.global_variables_initializer()
        sess.run(init)
        sess.run(tf.initialize_local_variables())
        total_batch = int((X.shape[0] + batch_size - 1) / batch_size)

        # Training cycle
        for epoch in range(self.training_epochs):
            avg_cost = 0.

            print total_batch
            # Loop over all batches
            for i in range(total_batch):
                batch_xs, batch_ys = X[i * batch_size:(i + 1) * batch_size], y[i * batch_size:(i + 1) * batch_size]
                # Run optimization op (backprop) and cost op (to get loss value)
                temp = coo_matrix(batch_xs)
                _, c, preds = sess.run([optimizer, cost, pred],
                                       feed_dict={input: (np.array([temp.row, temp.col]).T, temp.data, temp.shape),
                                                  output: batch_ys})
                # Compute average loss
                avg_cost += c / total_batch
                print 'Round', i
                # Display logs per epoch step
            if (epoch + 1) % self.display_step == 0:
                print("Epoch:", '%04d' % (epoch + 1), "cost=", "{:.9f}".format(avg_cost))

            temp = coo_matrix(eval[0])  # Test model

            print sess.run(self.roc_score,
                           feed_dict={self.input: (np.array([temp.row, temp.col]).T, temp.data, temp.shape),
                                      self.output: eval[1]})
        print("Optimization Finished!")

    pass

    def predict(self, X):
        pass

    def evaluate(self, X, y):
        sess = self.session
        temp = coo_matrix(X)  # Test model
        print sess.run(self.roc_score, feed_dict={self.input: (np.array([temp.row, temp.col]).T, temp.data, temp.shape),
                                                  self.output: y})

        print 'Predict:', sess.run(self.pred,
                                   feed_dict={self.input: (np.array([temp.row, temp.col]).T, temp.data, temp.shape)})
