#!/usr/bin/env python
# -- coding:utf-8 --

import sys

sys.path.append('..')
sys.path.append('../..')

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import log_loss
import framework
import lightgbm as lgb
import tools
import pandas as pd
import numpy as np
import zipfile


def read_set(set_name, feature_files, features):
    data_sets = [framework.load(name) for name in set_name.split(',')]
    X = pd.concat(
        [data_set.read_dense_feature(feature_files, features + [])
         for data_set in data_sets])
    Y = pd.concat([data_set.y for data_set in data_sets])
    return X, Y


def submit(df, filename):
    df.sort_values("click_id", inplace=True)
    df['is_attributed'] = df['pred']
    df[['click_id', 'is_attributed']].to_csv(filename, index=False)


def day_weight(x):
    return 1


def run(train_name, test_name, feature_files, submit_file=None, select_feature='xgb.f'):
    metrics = 'auc'
    params = {
        'boosting_type': 'gbdt',
        'objective': 'binary',
        'metric': metrics,
        'learning_rate': 0.1,
        # 'is_unbalance': 'true',  #because training data is unbalance (replaced with scale_pos_weight)
        'num_leaves': 63,  # we should let it be smaller than 2^(max_depth)
        'max_depth': -1,  # -1 means no limit
        'min_child_samples': 100,  # Minimum number of data need in a child(min_data_in_leaf)
        'max_bin': 100,  # Number of bucketed bin for feature values
        'subsample': 0.7,  # Subsample ratio of the training instance.
        'subsample_freq': 1,  # frequence of subsample, <=0 means no enable
        'colsample_bytree': 0.9,  # Subsample ratio of columns when constructing each tree.
        'nthread': -1,
        'verbose': 0,
        'scale_pos_weight': 20  # because training data is extremely unbalanced
    }

    features = tools.read_feature(select_feature)
    feature_base = features
    print len(features)
    categorical_features = filter(lambda x: '_cat_' in str(x), features)
    print features

    trainX, trainY = read_set(train_name, feature_files, features)
    testX, testY = read_set(test_name, feature_files, features)

    trainX.fillna(-1, downcast='infer', inplace=True)
    testX.fillna(-1, downcast='infer', inplace=True)

    dtrain = lgb.Dataset(trainX[features].values, label=trainY['label'].values,
                         feature_name=features, categorical_feature=categorical_features, )
    dtest = lgb.Dataset(testX[features].values, label=testY['label'].values, feature_name=features,
                        categorical_feature=categorical_features)
    del trainX
    if framework.IS_LOCAL:
        del testX

    evals_results = {}

    model = lgb.train(params,
                      dtrain,
                      valid_sets=[dtrain, dtest],
                      valid_names=['train', 'test'],
                      evals_result=evals_results,
                      num_boost_round=1000 if framework.IS_LOCAL else 100,
                      early_stopping_rounds=30 if framework.IS_LOCAL else None,
                      verbose_eval=10, )

    n_estimators = model.best_iteration
    print("\nModel Report")
    print("n_estimators : ", n_estimators)
    print(metrics + ":", evals_results['test'][metrics][n_estimators - 1])

    if not framework.IS_LOCAL and submit_file is not None:
        testY['pred'] = model.predict(testX[features].values)
        print testY['pred']
        submit(testY, submit_file)

    return model


if __name__ == '__main__':
    if framework.IS_LOCAL:
        features = [
            'ad_label_stat', 'user_label_stat', 'user_label_stat1',  # 'ad_cat', 'user_cat',
        ]
    else:
        features = [
            'ad_label_stat', 'user_label_stat',  # 'ad_cat', 'user_cat',
        ]

    run(sys.argv[1], sys.argv[2], features, None if len(sys.argv) <= 3 else sys.argv[3],
        select_feature='xgb.f' if framework.IS_LOCAL else 'xgb_online.f' if len(sys.argv) <= 4 else sys.argv[4])
