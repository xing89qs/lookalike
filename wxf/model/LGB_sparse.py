#!/usr/bin/env python
# -- coding:utf-8 --

import sys

sys.path.append('..')
sys.path.append('../..')

import framework
import lightgbm as lgb
import numpy as np
import argparse
import gc
import zipfile
import os
from sklearn import metrics


def read_set(set_name, features):
    set_name = set_name.split(',')
    X, Y = framework.concat([framework.load(data) for data in set_name], features)
    # framework.dump_svmlight_file(X, Y['label'].as_matrix(), ','.join(set_name) + '.svm')
    return X, Y


def submit(df, filename):
    df['score'] = df['pred'].apply(lambda x: np.round(x, 6))
    df[['aid', 'uid', 'score']].to_csv(filename, index=False)
    with zipfile.ZipFile(filename + '.zip', "w") as fout:
        fout.write(filename, compress_type=zipfile.ZIP_DEFLATED)


def train(train_name, test_name=None, eval_name=None, feature_files=None, model_file=None, output_file=None):
    metrics = 'auc'
    params = {
        'boosting_type': 'gbdt',
        'objective': 'binary',
        'metric': metrics,
        'learning_rate': 0.1,
        # 'is_unbalance': 'true',  #because training data is unbalance (replaced with scale_pos_weight)
        'num_leaves': 63,  # we should let it be smaller than 2^(max_depth)
        'max_depth': -1,  # -1 means no limit
        'min_child_samples': 100,  # Minimum number of data need in a child(min_data_in_leaf)
        'max_bin': 100,  # Number of bucketed bin for feature values
        'subsample': 0.7,  # Subsample ratio of the training instance.
        'subsample_freq': 1,  # frequence of subsample, <=0 means no enable
        'colsample_bytree': 0.9,  # Subsample ratio of columns when constructing each tree.
        'nthread': -1,
        'verbose': 0,
        'scale_pos_weight': 20  # because training data is extremely unbalanced
    }

    trainX, trainY = read_set(train_name, feature_files)
    evalX, evalY = None, None

    if eval_name is not None:
        evalX, evalY = read_set(eval_name, feature_files)

    dtrain = lgb.Dataset(trainX, label=trainY['label'].values)
    deval = None

    if eval_name is not None:
        deval = lgb.Dataset(evalX, label=evalY['label'].values)
    del trainX
    del trainY
    if eval_name is not None:
        del evalX
        del evalY
    gc.collect()

    evals_results = {}

    model = lgb.train(params,
                      dtrain,
                      valid_sets=[dtrain, deval] if eval_name is not None else [dtrain],
                      valid_names=['train', 'eval'] if eval_name is not None else ['train'],
                      evals_result=evals_results,
                      num_boost_round=20 if framework.IS_LOCAL else 500,
                      early_stopping_rounds=30 if framework.IS_LOCAL else None,
                      verbose_eval=10, )

    n_estimators = model.best_iteration
    print("\nModel Report")
    print("n_estimators : ", n_estimators)
    if deval is not None:
        print(metrics + ":", evals_results['eval'][metrics][n_estimators - 1])

    if model_file is not None:
        model.save_model(model_file)

    testX, testY = None, None
    if test_name is not None:
        testX, testY = read_set(test_name, feature_files)
    dtest = None
    if test_name is not None:
        dtest = lgb.Dataset(testX, label=testY['label'].values)

    if output_file is not None and dtest is not None:
        testY['pred'] = model.predict(testX)
        submit(testY, output_file)

    return model


def test(test_name, feature_files=None, model_file=None, output_file=None):
    testX, testY = None, None
    if test_name is not None:
        testX, testY = read_set(test_name, feature_files)
    dtest = lgb.Dataset(testX, label=testY['label'].values)

    if model_file is None or not os.path.exists(model_file):
        raise IOError('Model file %s not exists! ' % (model_file,))
    model = lgb.Booster(model_file=model_file)

    if dtest is not None:
        testY['pred'] = model.predict(testX)
        if 'label' not in testY.columns:
            testY['label'] = 0
        if max(testY['label']) != 0:
            test_auc = metrics.roc_auc_score(testY['label'].as_matrix(), testY['pred'].as_matrix())
            print 'Auc:', test_auc
        if output_file is not None:
            submit(testY, output_file)
    pass


if __name__ == '__main__':
    if framework.IS_LOCAL:
        features = [
            'all_ad_ids', 'all_user_ids', 'user_feature_multi1', 'app_feature_multi', 'kw_feature_multi',
            'topic_feature_multi', 'interest_feature_multi', 'aid_uid_cnt_feature', 'ad_label_stat', 'user_label_stat',
        ]
    else:
        features = [
            'all_ad_ids', 'all_user_ids', 'user_feature_multi1', 'app_feature_multi', 'kw_feature_multi',
            'topic_feature_multi', 'interest_feature_multi',
        ]
    parser = argparse.ArgumentParser()
    parser.add_argument('--train', help='train_set', type=str)
    parser.add_argument('--test', help='test_set', type=str)
    parser.add_argument('--eval', help='eval_set', type=str)
    parser.add_argument('--output', help='output_file', type=str)
    parser.add_argument('--model', help='model_file', type=str)
    parser.add_argument('--feature', help='feature_file', type=str)

    args = parser.parse_args()

    if hasattr(args, 'train') and args.train is not None:
        train_set = args.train
        test_set = args.test if hasattr(args, 'test') else None
        eval_set = args.eval if hasattr(args, 'eval') else None
        feature_file = args.feature if hasattr(args, 'feature') else None
        model_file = args.model if hasattr(args, 'model') else None
        output_file = args.model if hasattr(args, 'output') else None
        train(train_set, test_name=test_set, eval_name=eval_set, feature_files=features, model_file=model_file,
              output_file=output_file)
        pass
    elif hasattr(args, 'test') and args.test is not None:
        test_set = args.test
        print os.path.exists(args.model)
        with open(args.model, 'r') as f:
            for l in f:
                print l
        if not hasattr(args, 'model') or not os.path.exists(args.model):
            raise IOError('Model file not specified!')
        model_file = args.model
        output_file = args.model if hasattr(args, 'output') else None
        test(test_name=test_set, model_file=model_file, feature_files=features, output_file=output_file)
        pass
    else:
        raise AssertionError('must contain train or test!')
