#!/usr/bin/env python
# -- coding:utf-8 --

import sys

sys.path.append('..')
sys.path.append('../..')
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import auc, roc_curve
import framework
import xlearn as xl
from wxf.model.fm.fm import FM
from scipy.sparse import csr_matrix, coo_matrix
from sklearn.preprocessing import MinMaxScaler

features = ['all_ad_ids', 'all_user_ids', 'user_feature_multi1', 'app_feature_multi', 'kw_feature_multi',
            'topic_feature_multi', 'interest_feature_multi', ]

lr = FM()
trainX, trainY = framework.concat([framework.load(data) for data in ['train_0']], features)
test_set = framework.load('eval_0')
print 'Load done!'

lr.fit(trainX, trainY['label'].as_matrix(), eval=(test_set[features], test_set['label'].as_matrix()))
lr.evaluate(test_set[features], test_set['label'].as_matrix())
