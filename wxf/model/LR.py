#!/usr/bin/env python
# -- coding:utf-8 --

import sys

sys.path.append('..')
sys.path.append('../..')
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import auc, roc_curve
import framework
import xlearn as xl
from wxf.model.lr.lr import LR
from scipy.sparse import csr_matrix, coo_matrix
from sklearn.preprocessing import MinMaxScaler
from sklearn import metrics

features = ['all_ad_ids', 'all_user_ids', 'user_feature_multi1', 'app_feature_multi', 'kw_feature_multi',
            'topic_feature_multi', 'interest_feature_multi', ]

# lr = xl.LRModel()
lr = LR()
trainX, trainY = framework.concat([framework.load(data) for data in ['train_0']], features)
test_set = framework.load('eval_0')
print 'Load done!'


lr.fit(trainX, trainY['label'].as_matrix())
lr.evaluate(test_set[features], test_set['label'].as_matrix())
