#!/usr/bin/env python
# -- coding:utf-8 --

import sys

sys.path.append('..')
sys.path.append('../..')

import framework
import pandas as pd
from scipy.sparse import csr_matrix
from sklearn.preprocessing import OneHotEncoder
from sklearn.feature_extraction.text import CountVectorizer
import numpy as np


def tokenizer(x):
    return x.split('-')


def extract_features(set_name, f_index):
    dataset = framework.load(set_name)
    NEED_LABEL_SET = [2, 4, ]

    DTYPES = {
        'LBS': 'object',
        'age': 'int8',
        'carrier': 'int8',
        'consumptionAbility': 'int8',
        'ct': 'str',
        'education': 'int8',
        'gender': 'int8',
        'house': 'object',
        'marriageStatus': 'object',
        'os': 'str',
        'uid': 'int32',
        'appIdAction': 'str',
        'appIdInstall': 'str',
        'interest1': 'str',
        'interest2': 'str',
        'interest3': 'str',
        'interest4': 'str',
        'interest5': 'str',
        'kw1': 'str',
        'kw2': 'str',
        'kw3': 'str',
        'topic1': 'str',
        'topic2': 'str',
        'topic3': 'str',
    }
    if int(f_index) in NEED_LABEL_SET:
        if set_name == 'test':
            labelset = framework.load('train')
        elif set_name == 'train_0':
            labelset = dataset
        else:
            labelset = framework.load('train_0')
    DATA_ROOT = '../../data'
    print '读入dataset %s , 共 %d 条样本' % (set_name, len(dataset.y))

    def featureV1():
        ad_frame = pd.read_csv('%s/adFeature.csv' % (DATA_ROOT,))
        print ad_frame.columns
        frame_with_id = pd.merge(dataset.y, ad_frame, how='left')
        print frame_with_id

        # 广告ID, 这里要注意所有ID特征都要从ad.csv, user.csv这些文件里面的来做fit, 不能直接用dataset里的 不然标号不统一了
        AD_IDS = ['advertiserId', 'campaignId', 'creativeId', 'creativeSize', 'adCategoryId', 'productId',
                  'productType']
        results_features = []
        for ad_id in AD_IDS:
            encoder = OneHotEncoder()
            encoder.fit(ad_frame[ad_id].values.reshape(-1, 1))
            ret = csr_matrix(encoder.transform(frame_with_id[ad_id].values.reshape(-1, 1)), dtype=np.int8)
            results_features.append(ret)
        dataset.write_feature('all_ad_ids', results_features)

    def featureV2():
        ad_frame = pd.read_csv('%s/adFeature.csv' % (DATA_ROOT,))
        frame_with_id = pd.merge(pd.read_csv('%s/train.csv' % (DATA_ROOT,)), ad_frame, how='left')
        v2_frame = pd.merge(dataset.y, ad_frame, how='left')
        for id in ['advertiserId', 'campaignId', 'creativeId', 'creativeSize', 'adCategoryId', 'productId',
                   'productType', 'aid']:
            frame_temp = frame_with_id.groupby([id], as_index=False)['label'].agg(
                {'_' + id + '_count': len, '_' + id + '_sum': np.sum, '_' + id + '_avg': np.mean})
            v2_frame = pd.merge(v2_frame, frame_temp, how='left')
        v2_frame.fillna(-1, downcast='infer', inplace=True)
        feature_list = [x for x in v2_frame.columns if str(x).startswith('_')]
        dataset.write_dense_feature('ad_label_stat', csr_matrix(v2_frame[feature_list].values),
                                    fmap=feature_list)

    pass

    def featureV3():
        user_single_frame = pd.read_csv('%s/userFeatureSingle.csv' % (DATA_ROOT,), dtype=DTYPES)
        user_single_frame.fillna(0, downcast='infer', inplace=True)
        print user_single_frame.dtypes
        print user_single_frame.columns
        frame_with_id = pd.merge(dataset.y, user_single_frame, how='left')
        print frame_with_id

        # 用户ID
        USER_IDS = ['LBS', 'age', 'carrier', 'consumptionAbility', 'education', 'gender', 'house', ]
        results_features = []
        for user_id in USER_IDS:
            encoder = OneHotEncoder()
            encoder.fit(user_single_frame[user_id].values.reshape(-1, 1))
            ret = csr_matrix(encoder.transform(frame_with_id[user_id].values.reshape(-1, 1)), dtype=np.int8)
            results_features.append(ret)
        dataset.write_feature('all_user_ids', results_features)

    def featureV4():
        user_single_frame = pd.read_csv('%s/userFeatureSingle.csv' % (DATA_ROOT,), dtype=DTYPES)
        user_single_frame.fillna(0, downcast='infer', inplace=True)
        frame_with_id = pd.merge(pd.read_csv('%s/train.csv' % (DATA_ROOT,)), user_single_frame, how='left')
        v4_frame = pd.merge(dataset.y, user_single_frame, how='left')
        for id in ['LBS', 'age', 'carrier', 'consumptionAbility', 'education', 'gender', 'house']:
            frame_temp = frame_with_id.groupby([id], as_index=False)['label'].agg(
                {'_' + id + '_avg': np.mean, '_' + id + '_sum': np.sum, '_' + id + '_count': len})
            v4_frame = pd.merge(v4_frame, frame_temp, how='left')
        v4_frame.fillna(-1, downcast='infer', inplace=True)
        feature_list = [x for x in v4_frame.columns if str(x).startswith('_')] + ['uid']
        dataset.write_dense_feature('user_label_stat', csr_matrix(v4_frame[feature_list].values),
                                    fmap=feature_list)

    def featureV5():
        user_single_frame = pd.read_csv('%s/userFeatureSingle.csv' % (DATA_ROOT,),
                                        usecols=['os', 'marriageStatus', 'ct', 'uid'], dtype=DTYPES)
        user_single_frame.fillna(0, downcast='infer', inplace=True)
        v5_frame = pd.merge(dataset.y, user_single_frame, how='left')
        results_features = []
        for id in ['os', 'marriageStatus', 'ct']:
            if dataset.exists_common_model(id + '_cv.model'):
                cv = dataset.load_common_model(id + '_cv.model')
            else:
                cv = CountVectorizer(tokenizer=tokenizer)
                cv.fit(user_single_frame[id].apply(str).as_matrix())
                dataset.save_common_model(cv, id + '_cv.model')
            ret = csr_matrix(cv.transform(v5_frame[id].apply(str).as_matrix()), dtype=np.int8)
            results_features.append(ret)
        dataset.write_feature('user_feature_multi1', results_features)

    pass

    def featureV6():
        ad_frame = pd.read_csv('%s/adFeature.csv' % (DATA_ROOT,))
        v6_frame = pd.merge(dataset.y, ad_frame, how='left')
        for id in ['advertiserId', 'campaignId', 'creativeId', 'creativeSize', 'adCategoryId', 'productId',
                   'productType']:
            v6_frame['_cat_' + id] = v6_frame[id]
        v6_frame.fillna(-1, downcast='infer', inplace=True)
        feature_list = [x for x in v6_frame.columns if str(x).startswith('_')] + ['aid']
        dataset.write_dense_feature('ad_cat', csr_matrix(v6_frame[feature_list].values),
                                    fmap=feature_list)

    pass

    def featureV7():
        user_single_frame = pd.read_csv('%s/userFeatureSingle.csv' % (DATA_ROOT,), dtype=DTYPES)
        user_single_frame.fillna(0, downcast='infer', inplace=True)
        v7_frame = pd.merge(dataset.y, user_single_frame, how='left')
        for id in ['LBS', 'age', 'carrier', 'consumptionAbility', 'education', 'gender', 'house']:
            v7_frame['_cat_' + id] = v7_frame[id]
        v7_frame.fillna(-1, downcast='infer', inplace=True)
        feature_list = [x for x in v7_frame.columns if str(x).startswith('_')] + ['uid']
        dataset.write_dense_feature('user_cat', csr_matrix(v7_frame[feature_list].values),
                                    fmap=feature_list)

    def featureV8():
        user_multi_frame = pd.read_csv('%s/userFeatureMulti.csv' % (DATA_ROOT,),
                                       usecols=['appIdAction', 'appIdInstall', 'uid'], dtype=DTYPES)
        user_multi_frame.fillna('0', downcast='infer', inplace=True)
        print user_multi_frame.dtypes
        print user_multi_frame.columns
        v8_frame = pd.merge(dataset.y, user_multi_frame, how='left')
        results_features = []
        for id in ['appIdAction', 'appIdInstall', ]:
            if dataset.exists_common_model(id + '_cv.model'):
                cv = dataset.load_common_model(id + '_cv.model')
            else:
                cv = CountVectorizer(tokenizer=tokenizer)
                cv.fit(user_multi_frame[id].apply(str).as_matrix())
                dataset.save_common_model(cv, id + '_cv.model')
            ret = csr_matrix(cv.transform(v8_frame[id].apply(str).as_matrix()), dtype=np.int8)
            results_features.append(ret)
        dataset.write_feature('app_feature_multi', results_features)

    pass

    def featureV9():
        user_multi_frame = pd.read_csv('%s/userFeatureMulti.csv' % (DATA_ROOT,),
                                       usecols=['kw1', 'kw2', 'kw3', 'uid'], dtype=DTYPES)
        user_multi_frame.fillna('0', downcast='infer', inplace=True)
        v9_frame = pd.merge(dataset.y, user_multi_frame, how='left')
        results_features = []
        for id in ['kw1', 'kw2', 'kw3']:
            if dataset.exists_common_model(id + '_cv.model'):
                cv = dataset.load_common_model(id + '_cv.model')
            else:
                cv = CountVectorizer(tokenizer=tokenizer)
                cv.fit(user_multi_frame[id].apply(str).as_matrix())
                dataset.save_common_model(cv, id + '_cv.model')
            results_features.append(cv.transform(v9_frame[id].apply(str).as_matrix()))
        dataset.write_feature('kw_feature_multi', results_features)

    def featureV10():
        user_multi_frame = pd.read_csv('%s/userFeatureMulti.csv' % (DATA_ROOT,),
                                       usecols=['topic1', 'topic2', 'topic3', 'uid'], dtype=DTYPES)
        user_multi_frame.fillna('0', downcast='infer', inplace=True)
        v10_frame = pd.merge(dataset.y, user_multi_frame, how='left')
        results_features = []
        for id in ['topic1', 'topic2', 'topic3']:
            if dataset.exists_common_model(id + '_cv.model'):
                cv = dataset.load_common_model(id + '_cv.model')
            else:
                cv = CountVectorizer(tokenizer=tokenizer)
                cv.fit(user_multi_frame[id].apply(str).as_matrix())
                dataset.save_common_model(cv, id + '_cv.model')
            results_features.append(cv.transform(v10_frame[id].apply(str).as_matrix()))
        dataset.write_feature('topic_feature_multi', results_features)

    def featureV11():
        user_multi_frame = pd.read_csv('%s/userFeatureMulti.csv' % (DATA_ROOT,),
                                       usecols=['interest1', 'interest2', 'interest3', 'interest4', 'interest5', 'uid'],
                                       dtype=DTYPES)
        user_multi_frame.fillna('0', downcast='infer', inplace=True)
        v11_frame = pd.merge(dataset.y, user_multi_frame, how='left')
        results_features = []
        for id in ['interest1', 'interest2', 'interest3', 'interest4', 'interest5']:
            if dataset.exists_common_model(id + '_cv.model'):
                cv = dataset.load_common_model(id + '_cv.model')
            else:
                cv = CountVectorizer(tokenizer=tokenizer)
                cv.fit(user_multi_frame[id].apply(str).as_matrix())
                dataset.save_common_model(cv, id + '_cv.model')
            results_features.append(cv.transform(v11_frame[id].apply(str).as_matrix()))
        dataset.write_feature('interest_feature_multi', results_features)

    def featureV12():
        all_id_frame = pd.concat(
            [pd.read_csv('%s/%s.csv' % (DATA_ROOT, x))[['uid', 'aid']] for x in ['train', 'test1']])
        uid_frame = all_id_frame.groupby('uid', as_index=False)['aid'].agg({'_uid_cnt': len})
        aid_frame = all_id_frame.groupby('aid', as_index=False)['uid'].agg({'_aid_cnt': len})
        v12_frame = dataset.y[['uid', 'aid']]
        v12_frame = pd.merge(v12_frame, uid_frame, how='left')
        v12_frame = pd.merge(v12_frame, aid_frame, how='left')
        v12_frame.fillna(0, downcast='infer', inplace=True)
        results_features = []
        results_features.append(csr_matrix(v12_frame[['_uid_cnt']].as_matrix(), dtype=np.int32))
        results_features.append(csr_matrix(v12_frame[['_aid_cnt']].as_matrix(), dtype=np.int32))
        dataset.write_feature('aid_uid_cnt_feature', results_features)

    def featureV13():
        user_multi_frame = pd.read_csv('%s/userFeatureMulti.csv' % (DATA_ROOT,),
                                       usecols=['appIdAction', 'appIdInstall', 'uid'], dtype=DTYPES)
        user_multi_frame.fillna('0', downcast='infer', inplace=True)
        frame_with_id = pd.merge(pd.read_csv('%s/train.csv') % (DATA_ROOT,), user_multi_frame, how='left')
        v13_frame = pd.merge(dataset.y, user_multi_frame, how='left')
        for id in ['appIdAction', 'appIdInstall']:
            new_frame = pd.read_csv
            frame_temp = frame_with_id.groupby([id], as_index=False)['label'].agg(
                {'_' + id + '_count': len, '_' + id + '_sum': np.sum, '_' + id + '_avg': np.mean})
            v2_frame = pd.merge(v2_frame, frame_temp, how='left')
        v2_frame.fillna(-1, downcast='infer', inplace=True)
        feature_list = [x for x in v2_frame.columns if str(x).startswith('_')]
        dataset.write_dense_feature('ad_label_stat', csr_matrix(v2_frame[feature_list].values),
                                    fmap=feature_list)

    eval('featureV' + str(f_index))()
    pass


if __name__ == '__main__':
    for i in xrange(len(sys.argv[1].split(','))):
        extract_features(sys.argv[1].split(',')[i], sys.argv[2])
