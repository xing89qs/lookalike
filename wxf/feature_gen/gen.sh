#!/bin/bash

if [ "`uname`" = "Darwin" ];then
# local
    python2.7 gen_feature.py train_0 $1
    python2.7 gen_feature.py eval_0 $1
else
    python2.7 gen_feature.py train_0 $1
    python2.7 gen_feature.py eval_0 $1
    python2.7 gen_feature.py train $1
    python2.7 gen_feature.py test $1
fi


