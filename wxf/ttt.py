#!/usr/bin/env python
# -- coding:utf-8 --

from sklearn.feature_extraction.text import CountVectorizer


def tokenizer(x):
    return x.split('-')


cv = CountVectorizer(tokenizer=tokenizer)

cv.fit(['23333-44444', '2222-44444', '0'])
print cv.vocabulary_

print cv.transform(['23333-44444-2222-0'])

import pandas as pd

frame = pd.read_csv('../data/userFeatureSingle.csv')

for c in frame.columns:
    print c, max(frame[c]), min(frame[c])
