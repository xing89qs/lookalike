#!/usr/bin/env python
# -- coding:utf-8 --

import numpy as np
from scipy.sparse import csr_matrix
import itertools
import datetime
import math
import time
import argparse


def save_sparse_csr(filename, mat):
    np.savez(filename, data=mat.data, indices=mat.indices,
             indptr=mat.indptr, shape=mat.shape)


def save_dense_csr(filename, mat, fmap):
    np.savez(filename, data=mat.data, indices=mat.indices,
             indptr=mat.indptr, shape=mat.shape, fmap=fmap)


def load_sparse_csr(filename):
    loader = np.load(filename)
    return csr_matrix((loader['data'], loader['indices'], loader['indptr']),
                      shape=loader['shape'], dtype=np.float32)


def load_dense_csr(filename):
    loader = np.load(filename)
    return csr_matrix((loader['data'], loader['indices'], loader['indptr']),
                      shape=loader['shape']), loader['fmap']


def read_feature(file_path):
    with open(file_path) as f:
        features = []
        cur_feature = ""
        try:
            for line in f:
                line = line.strip()
                if line == "" or str(line).startswith("#"):
                    continue
                if line.startswith('$'):
                    if cur_feature == "":
                        raise Exception('特征不能为空!')
                    key = line[1:].split('=')[0]
                    values = line[1:].split('=')[1].split(',')
                    features[-1][1][key] = values
                elif line.startswith('_'):
                    cur_feature = line
                    features.append((cur_feature, {}))
                else:
                    raise Exception('未知格式 %s' % line)
            feature_set = []
            for f, key_values in features:
                keys = list(key_values.keys())
                for x in itertools.product(*key_values.values()):
                    feature_name = f
                    for i in range(len(x)):
                        feature_name = str(feature_name).replace('${' + keys[i] + '}', x[i])
                    feature_set.append(feature_name)
            return feature_set
        except Exception as e:
            print(e.args)
    return []


_MAP = {}


def str2time_stamp(_str, re='%Y-%m-%d %H:%M:%S'):
    if _str in _MAP:
        return _MAP[_str]
    ret = int(time.mktime(time.strptime(_str, re)))
    _MAP[_str] = ret
    return ret


def time_stamp2str(stamp, re='%Y-%m-%d %H:%M:%S'):
    return time.strftime(re, time.localtime(stamp))


def time_diff(day_str1, day_str2, time_regex='%Y-%m-%d%H%M%S'):
    '''
        计算day_str1和day_str2的日期差
    '''
    try:
        day_str1 = int(day_str1)
        day_str2 = int(day_str2)
        day_str1 = '%08d' % (day_str1)
        day_str2 = '%08d' % (day_str2)
        day_str1 = '2016-01-' + str(day_str1)[0:2] + ' ' + str(day_str1)[2:4] + ':' + str(day_str1)[4:6] + ':' + str(
            day_str1)[6:8]
        day_str2 = '2016-01-' + str(day_str2)[0:2] + ' ' + str(day_str2)[2:4] + ':' + str(day_str2)[4:6] + ':' + str(
            day_str2)[6:8]
        return math.fabs(str2time_stamp(day_str1) - str2time_stamp(day_str2))
    except:
        return -1


def get_args():
    parser = argparse.ArgumentParser()
